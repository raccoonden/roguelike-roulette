**Description**
A 'Roguelike Roulette' wheel, utiliziing [Flask](https://flask.palletsprojects.com/en/2.2.x/) to create a slim UI, along with [Winwheel.js](http://dougtesting.net/winwheel/docs), a javascript roulette wheel. 

The roulette wheel's purpose is to create an equally weighted chance to have a 'Roguelike' title selected from a list of titles, in which the user would play a run within that game. Once the run ends, another game can be chosen from the list, and so on.

This Python project runs locally hosted websites to allow a user to add or remove games from the list, and then generating a roulette wheel from the list as a seperate window. The new window can then be imported into a streaming application, such as OBS, as a browser source so that it can be displayed on screen.

By default, the list of games is maintained entirely within memory while the python script is running. However, a pre-existing list of games can be imported for use. The list of games should be within a plain-text format, seperating games on a new line.

Roguelike titles aren't your thing? Choose another video game genre, or use the roulette wheel for any other content:
- a frequent viewer/chatter prize wheel
- funny things to do or challenges to undertake with new follows/subs/cheers
- choose what you'll have for lunch/dinner

**Usage**
- Add/Remove games/topics from the UI
- Perform Wheel Spin once desired list is complete
- Import pre-existing list of topics
- Import as browser source within OBS

**Report Bugs or Contact**
Direct message via Discord (MormonJeezy#1606)

I'm still learning and would love to improve in all aspects of coding! Please suggest any improvements; all serious recommendations will be considered.

For issues/bugs, please include:
- The issue in question
- Version number
- steps to reproduce the issue
- screenshot(s), and/or twitch clip url or VOD of issue occuring

