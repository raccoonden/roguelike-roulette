from flask import Flask, render_template, redirect, request, url_for
import os

app = Flask(__name__)

games = []

# function that will refresh the page to update the current list
@app.route('/list_games', methods=['GET'])
def list_games():
    return render_template('list_games.html', games=games)

# Add game to list, and then refresh the page
@app.route('/add_game', methods=['POST'])
def add_game():
    game_name = request.form['game_name']
    # if ui element was clicked, but no text within field, ignore
    if not game_name == '':
        games.append(game_name)
    # refresh page using list_games function
    return redirect(url_for('list_games'))

# import games file from ui
@app.route('/', methods=['GET', 'POST'])
def import_games():
    if request.method == 'POST':
        file = request.files['file']
        games_imported = file.read().decode('utf-8').splitlines()
        # clear pre-existing games that were added/imported
        games.clear()
        for game in games_imported:
            games.append(game.strip())
        print('Games imported')
        return redirect(url_for('list_games'))
    return render_template('list_games.html')

# remove game from list, and then refresh the page
@app.route('/remove_game', methods=['POST'])
def remove_game():
    game_name = request.form['game_name']
    try:
        games.remove(game_name)
    # if we're attempting to remove a game from the list that
    # doesn't already exist within it, for some reason
    except (remove_game.ValueError):
        print(f'Game name value {game_name} does not exist, skipping...')
    finally:
        # refresh page using list_games function
        return redirect(url_for('list_games'))

@app.route('/wheel', methods=['GET'])
def wheel():
        return render_template('wheel.html', games=games)

if __name__ == '__main__':
    # run site, with debug param
    app.run(debug=True, port=5002)